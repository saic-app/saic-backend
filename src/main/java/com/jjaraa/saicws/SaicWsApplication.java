package com.jjaraa.saicws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SaicWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SaicWsApplication.class, args);
	}

}
